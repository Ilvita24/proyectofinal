from flask import Flask, render_template, request, redirect, url_for
import psycopg2

# Crear una instancia de la aplicación Flask
app = Flask(__name__)

# Clase BaseCRUD para operaciones CRUD genéricas en una tabla específica
class BaseCRUD:
    def __init__(self, tabla):
# Inicializa la conexión a la base de datos y define la tabla
        self.tabla = tabla
        self.connection = psycopg2.connect(
            dbname="ProyectoFinal",
            user="postgres",
            password="ilviadua7",
            host="localhost"
        )
        self.cursor = self.connection.cursor()

    def __del__(self):
# Cierra la conexión a la base de datos y el cursor
        self.cursor.close()
        self.connection.close()

    def listar(self):
# Lista todos los registros de la tabla
        sql = f"SELECT * FROM {self.tabla}"
        self.cursor.execute(sql)
        result = self.cursor.fetchall()
        return result

    def buscar(self, id):
# Busca un registro específico por ID
        sql = f"SELECT * FROM {self.tabla} WHERE {self.tabla}_id=%s"
        self.cursor.execute(sql, (id,))
        result = self.cursor.fetchone()
        return result

    def agregar(self, *args):
    # Agrega un nuevo registro a la tabla
        placeholders = ', '.join(['%s'] * len(args))
        columnas = ', '.join(self.get_columns()[1:])  # Excluir el ID autogenerado
        sql = f"INSERT INTO {self.tabla} ({columnas}) VALUES ({placeholders})"
        self.cursor.execute(sql, args)
        self.connection.commit()

    def editar(self, id, **kwargs):
    # Edita un registro existente por ID
        columnas = list(kwargs.keys())
        valores = list(kwargs.values())
        campos = ', '.join([f"{col}=%s" for col in columnas])
        sql = f"UPDATE {self.tabla} SET {campos} WHERE {self.tabla}_id=%s"
        self.cursor.execute(sql, valores + [id])
        self.connection.commit()

    def eliminar(self, id):
    # Elimina un registro por ID
        sql = f"DELETE FROM {self.tabla} WHERE {self.tabla}_id=%s"
        self.cursor.execute(sql, [id])
        self.connection.commit()

    def get_columns(self):
        # Obtiene los nombres de las columnas de la tabla
        sql = f"SELECT column_name FROM information_schema.columns WHERE table_name='{self.tabla}' ORDER BY ordinal_position"
        self.cursor.execute(sql)
        columns = [row[0] for row in self.cursor.fetchall()]
        return columns

# Instancias de BaseCRUD para diferentes tablas
estudiante_crud = BaseCRUD("estudiante")
profesor_crud = BaseCRUD("profesor")
curso_crud = BaseCRUD("curso")

# Ruta para la página de inicio
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/profesor', methods=['GET', 'POST'])
def profesor():
    profesor_a_editar = None

# Manejo de formularios POST para agregar, editar o eliminar profesores

    if 'agregar' in request.form:
        if request.method == 'POST':
            if 'agregar' in request.form:
            # Datos del formulario para agregar un profesor
                nombre_profesor = request.form.get('nombre_profesor')
                apellido_profesor = request.form.get('apellido_profesor')
                telefono_profesor = request.form.get('telefono_profesor')
                correo = request.form.get('correo')
            if nombre_profesor and apellido_profesor and telefono_profesor and correo:
                profesor_crud.agregar(nombre_profesor, apellido_profesor, telefono_profesor, correo)

        elif 'editar' in request.form:
        # Datos del formulario para editar un profesor existente
            profesor_id = request.form.get('profesor_id')
            nombre_profesor = request.form.get('nombre_profesor')
            apellido_profesor = request.form.get('apellido_profesor')
            telefono_profesor = request.form.get('telefono_profesor')
            correo = request.form.get('correo')
            if profesor_id and nombre_profesor and apellido_profesor and telefono_profesor and correo:
                profesor_crud.editar(profesor_id, nombre_profesor=nombre_profesor, apellido_profesor=apellido_profesor, telefono_profesor=telefono_profesor, correo=correo)
                # Puedes redirigir al usuario a la página principal o a alguna otra página
                return redirect(url_for('profesor'))

        elif 'eliminar' in request.form:
        # Datos del formulario para eliminar un profesor
            profesor_id = request.form.get('profesor_id')
            if profesor_id:
                profesor_crud.eliminar(profesor_id)

    elif 'id' in request.args:
        # Manejo de solicitudes GET para editar un profesor
        profesor_id = request.args['id']
        profesor_a_editar = profesor_crud.buscar(profesor_id)

# Listamos todos los profesores para mostrarlos en la página
    profesores = profesor_crud.listar()
    return render_template('profesor.html', profesores=profesores, profesor_a_editar=profesor_a_editar)

@app.route('/curso', methods=['GET', 'POST'])
def curso():
    curso_a_editar = None

    if request.method == 'POST':
        # Manejo de formularios POST para agregar, editar o eliminar cursos
        if 'agregar' in request.form:
            # Datos del formulario para agregar un curso
            nombre_curso = request.form.get('nombre_curso')
            if nombre_curso:
                curso_crud.agregar(nombre_curso)

        elif 'editar' in request.form:
            # Datos del formulario para editar un curso existente
            curso_id = request.form.get('curso_id')
            nombre_curso = request.form.get('nombre_curso')
            if curso_id and nombre_curso:
                curso_crud.editar(curso_id, nombre_curso=nombre_curso)
                # Redirigimos a la página de curso
                return redirect(url_for('curso'))
        
        elif 'eliminar' in request.form:
            # Datos del formulario para eliminar un curso
            curso_id = request.form.get('curso_id')
            if curso_id:
                curso_crud.eliminar(curso_id)
                return redirect(url_for('curso'))


    # Manejo de solicitudes GET para editar un curso
    elif 'id' in request.args:
        curso_id = request.args['id']
        curso_a_editar = curso_crud.buscar(curso_id)
    
    # Listamos todos los cursos para mostrarlos en la página
    cursos = curso_crud.listar()

    return render_template('curso.html', cursos=cursos, curso_a_editar=curso_a_editar)

    
@app.route('/estudiante', methods=['GET', 'POST'])
def estudiante():
    estudiante_a_editar = None

    if request.method == 'POST':
    # Manejo de formularios POST para agregar, editar o eliminar estudiantes
        if 'agregar' in request.form:
            # Datos del formulario para agregar un estudiante
            nombres = request.form.get('nombres')
            apellidos = request.form.get('apellidos')
            grado = request.form.get('grado')
            seccion = request.form.get('seccion')
            codigo = request.form.get('codigo')
            encargado_nombre = request.form.get('encargado_nombre')
            telefono = request.form.get('telefono')
            sangre_tipo = request.form.get('sangre_tipo')
            alergia = request.form.get('alergia')
            discapacidad = request.form.get('discapacidad')
            # Comprobamos que todos los campos están presentes
            if nombres and apellidos and grado and seccion and codigo and encargado_nombre and telefono and sangre_tipo and alergia and discapacidad:
                estudiante_crud.agregar(nombres, apellidos, grado, seccion, codigo, encargado_nombre, telefono, sangre_tipo, alergia, discapacidad)

        elif 'editar' in request.form:
            # Datos del formulario para editar un estudiante existente
            estudiante_id = request.form.get('estudiante_id')
            nombres = request.form.get('nombres')
            apellidos = request.form.get('apellidos')
            grado = request.form.get('grado')
            seccion = request.form.get('seccion')
            codigo = request.form.get('codigo')
            encargado_nombre = request.form.get('encargado_nombre')
            telefono = request.form.get('telefono')
            sangre_tipo = request.form.get('sangre_tipo')
            alergia = request.form.get('alergia')
            discapacidad = request.form.get('discapacidad')
            # Comprobamos que todos los campos están presentes
            if estudiante_id and nombres and apellidos and grado and seccion and codigo and encargado_nombre and telefono and sangre_tipo and alergia and discapacidad:
                estudiante_crud.editar(estudiante_id, nombres=nombres, apellidos=apellidos, grado=grado, seccion=seccion, codigo=codigo, encargado_nombre=encargado_nombre, telefono=telefono, sangre_tipo=sangre_tipo, alergia=alergia, discapacidad=discapacidad)
                # Redirigimos al usuario a la página de estudiantes después de la edición
                return redirect(url_for('estudiante'))

        elif 'eliminar' in request.form:
            estudiante_id = request.form.get('estudiante_id')
            print(request.form)
            # Comprobamos que el ID del estudiante está presente
            if estudiante_id:
                estudiante_crud.eliminar(estudiante_id)
                return redirect(url_for('estudiante'))

    # Manejo de solicitudes GET
    elif 'id' in request.args:
        estudiante_id = request.args['id']
        # Buscamos el estudiante por su ID para editarlo
        estudiante_a_editar = estudiante_crud.buscar(estudiante_id)

    # Listamos todos los estudiantes para mostrarlos en la página
    estudiantes = estudiante_crud.listar()
    # Renderizamos la plantilla con los datos necesarios
    return render_template('estudiante.html', estudiantes=estudiantes, estudiante_a_editar=estudiante_a_editar)

if __name__ == '__main__':
    # Ejecutar la aplicación en modo de depuración
    app.run(debug=True)
